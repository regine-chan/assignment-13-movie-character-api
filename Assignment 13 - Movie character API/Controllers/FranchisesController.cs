﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment_13___Movie_character_API.Models;
using AutoMapper;
using Assignment_13___Movie_character_API.Dtos;

namespace Assignment_13___Movie_character_API.Controllers
{
    /// <summary>
    /// Api controller for the Franchises table
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly FilmDbContext _context;
        private readonly IMapper _mapper;
        public FranchisesController(FilmDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// GET: api/Franchises
        /// Fetches all franchises in the Franchises table
        /// </summary>
        /// <returns>Task<ActionResult<IEnumerable<Franchise>>></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseDto>>> GetFranchises()
        {
            List<Franchise> franchises = await _context.Franchises.ToListAsync();
            if (franchises == null)
            {
                return NotFound();
            }
            List<FranchiseDto> franchiseDtos = new List<FranchiseDto>();
            foreach (Franchise franchise in franchises)
            {
                franchiseDtos.Add(_mapper.Map<FranchiseDto>(franchise));
            }
            return franchiseDtos;
        }

        /// <summary>
        /// GET: api/Franchises/5
        /// Fetches a franchise of <paramref name="id"/>
        /// Returns NotFound if no entity related to the id is found
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Task<ActionResult<Franchise>></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseDto>> GetFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseDto>(franchise);
        }

        /// <summary>
        /// GET: api/Franchises/5/Movies
        /// Fetches all movies related to the franchise
        /// Returns NotFound if no movies is found
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Task<ActionResult<IEnumerable<Movie>>></returns>
        [HttpGet("{id}/Movies")]
        public async Task<ActionResult<IEnumerable<MovieDto>>> GetFranchiseMovies(int id)
        {
            List<Movie> movies = await _context.Movies.Where(m => m.FranchiseId == id).Select(m => m).ToListAsync();

            if (movies == null)
            {
                return NotFound();
            }
            List<MovieDto> movieDtos = new List<MovieDto>();
            foreach (Movie movie in movies)
            {
                movieDtos.Add(_mapper.Map<MovieDto>(movie));
            }

            return movieDtos;
        }

        /// <summary>
        /// PUT: api/Franchises/5
        /// Updates a franchise
        /// If id and franchise isn't related a BadRequest is returned
        /// If franchise isn't found a NotFound is returned
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchise"></param>
        /// <returns>Task<IActionResult></returns>
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, Franchise franchise)
        {
            if (id != franchise.Id)
            {
                return BadRequest();
            }

            _context.Entry(franchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// POST: api/Franchises
        /// Creates and saves a new Franchise entity to the Franchises table
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns>Task<ActionResult<Franchise>></returns>
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFranchise", new { id = franchise.Id }, franchise);
        }

        /// <summary>
        /// DELETE: api/Franchises/5
        /// Removes a franchise by param <paramref name="id"/>
        /// Returs NotFound if there is no Franchise with the param <paramref name="id"/>
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Task<ActionResult<Franchise>></returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<Franchise>> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return franchise;
        }

        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }

        /// <summary>
        /// GET: api/Franchises/5/Characters
        /// Fetches all characters related to a franchise by <paramref name="id"/>
        /// Returns NotFound if no characters are found
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Task<ActionResult<IEnumerable<Character>>></returns>
        [HttpGet("{id}/Characters")]
        public async Task<ActionResult<IEnumerable<CharacterDto>>> GetFranchiseCharacters(int id)
        {
            // var characters = await _context.Franchises.Where(f => f.Id == id).SelectMany(f => f.Movies.SelectMany(m => m.MovieChracters.Select(mc => mc.Character))).ToListAsync();

            List<Character> characters = await _context.MovieCharacters.Where(mc => mc.Movie.FranchiseId == id).Select(mc => mc.Character).Distinct().ToListAsync();

            if (characters == null)
            {
                return NotFound();
            }
            List<CharacterDto> characterDtos = new List<CharacterDto>();
            foreach (Character character in characters)
            {
                characterDtos.Add(_mapper.Map<CharacterDto>(character));
            }

            return characterDtos;
        }
    }
}

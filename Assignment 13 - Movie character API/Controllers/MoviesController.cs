﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment_13___Movie_character_API.Models;
using AutoMapper;
using Assignment_13___Movie_character_API.Dtos;

namespace Assignment_13___Movie_character_API.Controllers
{
    /// <summary>
    /// Api controller for the Movies Table
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly FilmDbContext _context;
        private readonly IMapper _mapper;

        public MoviesController(FilmDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// GET: api/Movies
        /// Fetches all Movies in the Movies table
        /// </summary>
        /// <returns>Task<ActionResult<IEnumerable<Movie>>></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieDto>>> GetMovies()
        {
            List<Movie> movies = await _context.Movies.ToListAsync();
            if (movies == null)
            {
                return NotFound();
            }
            List<MovieDto> movieDtos = new List<MovieDto>();
            foreach (Movie movie in movies)
            {
                movieDtos.Add(_mapper.Map<MovieDto>(movie));
            }
            return movieDtos;
        }

        /// <summary>
        /// GET: api/Movies/5
        /// Fetches a movie matching the <paramref name="id"/>
        /// Returns NotFound if no matches are found
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Task<ActionResult<Movie>></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieDto>> GetMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieDto>(movie);
        }

        /// <summary>
        /// PUT: api/Movies/5
        /// Updates a Movies object by <paramref name="id"/> and <paramref name="movie"/>
        /// If the Id's from <paramref name="id"/> and <paramref name="movie"/> does not match a BadRequest is returned.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movie"></param>
        /// <returns>Task<IActionResult></returns>
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, Movie movie)
        {
            if (id != movie.Id)
            {
                return BadRequest();
            }

            _context.Entry(movie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// POST: api/Movies
        /// Creates a new Movie entity in the Movies table
        /// </summary>
        /// <param name="movie"></param>
        /// <returns>Task<ActionResult<Movie>></returns> 
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovie", new { id = movie.Id }, movie);
        }

        // DELETE: api/Movies/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Movie>> DeleteMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            return movie;
        }

        private bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }

        /// <summary>
        /// Fetches all Characters in a movie matching the <paramref name="id"/>
        /// Returns not found if no movies is found
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Task<ActionResult<IEnumerable<MovieCharacter>>></returns>
        // GET: api/Movies/5/Characters
        [HttpGet("{id}/Characters")]
        public async Task<ActionResult<IEnumerable<MovieCharacterDto>>> GetMovieCharacters(int id)
        {
            List<MovieCharacter> movieCharacters = await _context.MovieCharacters
                .Where(mc => mc.MovieId == id)
                .Select(mc => mc)
                .Include(mc => mc.Actor)
                .ToListAsync();

            if (movieCharacters == null)
            {
                return NotFound();
            }
            List<MovieCharacterDto> movieCharacterDtos = new List<MovieCharacterDto>();
            foreach (MovieCharacter movieCharacter in movieCharacters)
            {
                movieCharacterDtos.Add(_mapper.Map<MovieCharacterDto>(movieCharacter));
            }

            return movieCharacterDtos;
        }
    }
}

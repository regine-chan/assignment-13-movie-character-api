﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment_13___Movie_character_API.Models;
using AutoMapper;
using Assignment_13___Movie_character_API.Dtos;

namespace Assignment_13___Movie_character_API.Controllers
{
    /// <summary>
    /// Api controller for the Characters table
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class CharactersController : ControllerBase
    {
        private readonly FilmDbContext _context;
        private readonly IMapper _mapper;


        public CharactersController(FilmDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// GET: api/Characters
        /// Method to get all the characters in the Characters table
        /// </summary>
        /// <returns>Task<ActionResult<IEnumerable<Character>>></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterDto>>> GetCharacters()
        {
            List<Character> characters = await _context.Characters.ToListAsync();
            if (characters == null)
            {
                return NotFound();
            }
            List<CharacterDto> characterDtos = new List<CharacterDto>();
            foreach (Character character in characters)
            {
                characterDtos.Add(_mapper.Map<CharacterDto>(character));
            }
            return characterDtos;
        }

        /// <summary>
        /// GET: api/Characters/5
        /// Method to fetch a character from the characters table, matching the id param.
        /// Returns not found if there is no character by the <paramref name="id"/>
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Task<ActionResult<Character>></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterDto>> GetCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterDto>(character);
        }

        /// <summary>
        /// PUT: api/Characters/5
        /// Updates a character tuple from the Characters table
        /// Returns BadRequest if character does not match id param
        /// Returns NotFound if character is not found in the Characters table
        /// </summary>
        /// <param name="id"></param>
        /// <param name="character"></param>
        /// <returns>Task<IActionResult></returns>
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, Character character)
        {
            if (id != character.Id)
            {
                return BadRequest();
            }

            _context.Entry(character).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// POST: api/Characters
        /// Creates a new character entity and stores it in the Characters table
        /// </summary>
        /// <param name="character"></param>
        /// <returns>Task<ActionResult<Character>></returns>
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Character>> PostCharacter(Character character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCharacter", new { id = character.Id }, character);
        }

        /// <summary>
        /// DELETE: api/Characters/5
        /// Deletes a character by <paramref name="id"/>
        /// Returns NotFound if entity doesn't exist
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Task<ActionResult<Character>></returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<Character>> DeleteCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

            return character;
        }

        private bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.Id == id);
        }

        /// <summary>
        /// GET: api/Characters/5/Actors
        /// Fetches all the actors who have played a character
        /// Returns NotFound if no actors are found related to that character
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Task<ActionResult<IEnumerable<Actor>>></returns>
        [HttpGet("{id}/Actors")]
        public async Task<ActionResult<IEnumerable<ActorDto>>> GetCharacterActors(int id)
        {
            var actors = await _context.Characters.Where(c => c.Id == id).SelectMany(c => c.MovieChracters.Select(mc => mc.Actor)).ToListAsync();

            if (actors == null)
            {
                return NotFound();
            }
            List<ActorDto> actorDtos = new List<ActorDto>();
            foreach (Actor actor in actors)
            {
                actorDtos.Add(_mapper.Map<ActorDto>(actor));
            }

            return actorDtos;
        }
    }
}

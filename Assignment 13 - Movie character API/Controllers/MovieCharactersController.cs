using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment_13___Movie_character_API.Models;
using AutoMapper;
using Assignment_13___Movie_character_API.Dtos;
using Swashbuckle.Swagger.Annotations;

namespace Assignment_13___Movie_character_API.Controllers
{
    /// <summary>
    /// Api controller for the MovieCharacters table
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class MovieCharactersController : ControllerBase
    {
        private readonly FilmDbContext _context;
        private readonly IMapper _mapper;

        public MovieCharactersController(FilmDbContext context, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
        }

        /// <summary>
        /// GET: api/MovieCharacters
        /// Fetches all MovieCharacters from the MovieCharacters table
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieCharacterDto>>> GetMovieCharacters()
        {
            List<MovieCharacter> movieCharacters = await _context.MovieCharacters
            .Include(mc => mc.Actor)
            .Include(mc => mc.Movie)
            .Include(mc => mc.Character)
            .ToListAsync();

            List<MovieCharacterDto> movieCharacterDtos = new List<MovieCharacterDto>();
            foreach (MovieCharacter movieCharacter in movieCharacters)
            {
                movieCharacterDtos.Add(_mapper.Map<MovieCharacterDto>(movieCharacter));
            }
            return movieCharacterDtos;
        }

        /// <summary>
        /// Fetches a MovieCharacter <paramref name="movieId"/> and <paramref name="characterId"/>
        /// Returns NotFound if no MovieCharacter is found
        /// </summary>
        /// <param name="movieId"></param>
        /// <param name="characterId"></param>
        /// <returns>MovieCharacters</returns>
        [HttpGet("{movieId}&{characterId}")]
        public async Task<ActionResult<MovieCharacterDto>> GetMovieCharacter(int movieId, int characterId)
        {
            var movieCharacter = await _context.MovieCharacters
            .Select(mc => mc)
            .Where(mc => mc.MovieId == movieId)
            .Where(mc => mc.CharacterId == characterId)
            .Include(mc => mc.Actor)
            .Include(mc => mc.Character)
            .Include(mc => mc.Movie)
            .FirstAsync();

            if (movieCharacter == null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieCharacterDto>(movieCharacter);
        }

        /// <summary>
        /// Updates a MovieCharacter entity
        /// Returns BadRequest if <paramref name="id"/> and <paramref name="movieCharacter"/> does not match
        /// Returns NotFound if no entity with <paramref name="id"/> does not exist
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movieCharacter"></param>
        /// <returns>Task<IActionResult></returns>
        // PUT: api/MovieCharacters/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{movieId}&{characterId}")]
        public async Task<IActionResult> PutMovieCharacter(int movieId, int characterId, MovieCharacter movieCharacter)
        {
            if (characterId != movieCharacter.CharacterId || movieId != movieCharacter.CharacterId)
            {
                return BadRequest();
            }

            _context.Entry(movieCharacter).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieCharacterExists(movieId, characterId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// POST: api/MovieCharacters
        /// Creates a new entity in the MovieCharacters table
        /// Return a Conflict if a MovieCharacter with <paramref name="movieCharacter"/>.Id already exist
        /// </summary>
        /// <param name="movieCharacter"></param>
        /// <returns>Task<ActionResult<MovieCharacter>></returns>
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<MovieCharacter>> PostMovieCharacter(MovieCharacter movieCharacter)
        {
            _context.MovieCharacters.Add(movieCharacter);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (MovieCharacterExists(movieCharacter.MovieId, movieCharacter.CharacterId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetMovieCharacter", new { movieId = movieCharacter.MovieId, characterId = movieCharacter.CharacterId }, movieCharacter);
        }

        /// <summary>
        /// DELETE: api/MovieCharacters/5
        /// Removes a movie character by <paramref name="id"/>
        /// Returns NotFound if no MovieCharacter exists by <paramref name="id"/>
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Task<ActionResult<MovieCharacter>></returns>
        [HttpDelete("{movieId}&{characterId}")]
        public async Task<ActionResult<MovieCharacter>> DeleteMovieCharacter(int movieId, int characterId)
        {
            var movieCharacter = await _context.MovieCharacters.FindAsync(movieId, characterId);
            if (movieCharacter == null)
            {
                return NotFound();
            }

            _context.MovieCharacters.Remove(movieCharacter);
            await _context.SaveChangesAsync();

            return movieCharacter;
        }

        private bool MovieCharacterExists(int movieId, int characterId)
        {
            return _context.MovieCharacters.Find(movieId, characterId) != null;
        }
    }
}

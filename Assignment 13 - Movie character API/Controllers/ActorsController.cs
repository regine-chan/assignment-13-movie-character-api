﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment_13___Movie_character_API.Models;
using AutoMapper;
using Assignment_13___Movie_character_API.Dtos;

namespace Assignment_13___Movie_character_API.Controllers
{
    /// <summary>
    /// API controller to access the Actors table
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ActorsController : ControllerBase
    {
        private readonly FilmDbContext _context;
        private readonly IMapper _mapper;

        public ActorsController(FilmDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// GET: api/Actors
        /// Fetches all the actors currently in the Actors table
        /// </summary>
        /// <returns>Task<ActionResult<IEnumerable<Actor>>></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ActorDto>>> GetActors()
        {

            List<Actor> actors = await _context.Actors.ToListAsync();

            List<ActorDto> actorDtos = new List<ActorDto>();
            foreach (Actor actor in actors)
            {
                actorDtos.Add(_mapper.Map<ActorDto>(actor));
            }
            return actorDtos;
        }
        /// <summary>
        /// GET: api/Actors/5
        /// Fetches an actor from Id param
        /// If it doesn't exist it returns a NotFound (404) exception
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Task<ActionResult<Actor>></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<ActorDto>> GetActor(int id)
        {
            var actor = await _context.Actors.FindAsync(id);

            if (actor == null)
            {
                return NotFound();
            }

            return _mapper.Map<ActorDto>(actor);
        }

        /// <summary>
        /// PUT: api/Actors/5
        /// Updates an actor entity
        /// Returns BadRequest if id and actor differs
        /// Returns NotFound if actor does not exist
        /// </summary>
        /// <param name="id"></param>
        /// <param name="actor"></param>
        /// <returns>Task<IActionResult></returns>
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutActor(int id, Actor actor)
        {
            if (id != actor.Id)
            {
                return BadRequest();
            }

            _context.Entry(actor).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// POST: api/Actors
        /// Saves a new actor entity to the Actors table
        /// </summary>
        /// <param name="actor"></param>
        /// <returns>Task<ActionResult<Actor>></returns>
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Actor>> PostActor(Actor actor)
        {
            _context.Actors.Add(actor);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetActor", new { id = actor.Id }, actor);
        }

        /// <summary>
        /// DELETE: api/Actors/5
        /// Deletes an entity in the Actors table that matches the id param
        /// Returns NotFound if the actor doesn't exist in the table
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Task<ActionResult<Actor>></returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<Actor>> DeleteActor(int id)
        {
            var actor = await _context.Actors.FindAsync(id);
            if (actor == null)
            {
                return NotFound();
            }

            _context.Actors.Remove(actor);
            await _context.SaveChangesAsync();

            return actor;
        }

        private bool ActorExists(int id)
        {
            return _context.Actors.Any(e => e.Id == id);
        }

        /// <summary>
        /// GET: api/Actors/5/Characters
        /// Fetches the Characters an actor has played
        /// Returns NotFound if no characters are found related to that actor
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Task<ActionResult<IEnumerable<Character>>></returns>
        [HttpGet("{id}/Characters")]
        public async Task<ActionResult<IEnumerable<CharacterDto>>> GetActorCharacters(int id)
        {
            var characters = await _context.Actors.Where(a => a.Id == id).SelectMany(a => a.MovieCharacters.Select(mc => mc.Character)).ToListAsync();

            if (characters == null)
            {
                return NotFound();
            }

            List<CharacterDto> characterDtos = new List<CharacterDto>();
            foreach (Character character in characters)
            {
                characterDtos.Add(_mapper.Map<CharacterDto>(character));
            }

            return characterDtos;
        }

        /// <summary>
        /// GET: api/Actors/5/Movies
        /// Fetches all movies an actor has played in
        /// Returns NotFound if no movies are found related to that actor
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Task<ActionResult<IEnumerable<Movie>>></returns>
        [HttpGet("{id}/Movies")]
        public async Task<ActionResult<IEnumerable<MovieDto>>> GetActorMovies(int id)
        {
            var movies = await _context.Actors.Where(a => a.Id == id).SelectMany(a => a.MovieCharacters.Select(mc => mc.Movie)).ToListAsync();

            if (movies == null)
            {
                return NotFound();
            }
            List<MovieDto> movieDtos = new List<MovieDto>();
            foreach (Movie movie in movies)
            {
                movieDtos.Add(_mapper.Map<MovieDto>(movie));
            }

            return movieDtos;
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Assignment_13___Movie_character_API.Migrations
{
    public partial class Addedseedsformoviecharacter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Actors",
                columns: new[] { "Id", "Biography", "DOB", "FirstName", "Gender", "LastName", "OtherNames", "PlaceOfBirth" },
                values: new object[,]
                {
                    { 9, "Biography9", new DateTime(1967, 11, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "Mark", "Male", "Ruffalo", null, "Kenosha, Wisconsin" },
                    { 10, "Biography9", new DateTime(1969, 8, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), "Edward", "Male", "Norton", null, "Boston, Massachusetts" },
                    { 11, "Biography10", new DateTime(1968, 8, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), "Eric", "Male", "Bana", null, "Melbourne, Victoria" }
                });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "Alias", "Gender", "ImageLink", "Name" },
                values: new object[] { 1, "The Hulk", "Male", null, "Bruce Banner" });

            migrationBuilder.InsertData(
                table: "MovieCharacters",
                columns: new[] { "CharacterId", "MovieId", "ActorId", "PictureLink" },
                values: new object[] { 1, 5, 9, null });

            migrationBuilder.InsertData(
                table: "MovieCharacters",
                columns: new[] { "CharacterId", "MovieId", "ActorId", "PictureLink" },
                values: new object[] { 1, 1, 10, null });

            migrationBuilder.InsertData(
                table: "MovieCharacters",
                columns: new[] { "CharacterId", "MovieId", "ActorId", "PictureLink" },
                values: new object[] { 1, 2, 11, null });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "CharacterId", "MovieId" },
                keyValues: new object[] { 1, 1 });

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "CharacterId", "MovieId" },
                keyValues: new object[] { 1, 2 });

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "CharacterId", "MovieId" },
                keyValues: new object[] { 1, 5 });

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}

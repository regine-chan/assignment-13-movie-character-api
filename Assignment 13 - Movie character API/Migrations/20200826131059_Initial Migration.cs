﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Assignment_13___Movie_character_API.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Actors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    OtherNames = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    DOB = table.Column<DateTime>(nullable: false),
                    PlaceOfBirth = table.Column<string>(nullable: true),
                    Biography = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Actors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Characters",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Alias = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    ImageLink = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchises",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchises", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(nullable: true),
                    Genre = table.Column<string>(nullable: true),
                    ReleaseYear = table.Column<int>(nullable: false),
                    Director = table.Column<string>(nullable: true),
                    ImageLink = table.Column<string>(nullable: true),
                    TrailerLink = table.Column<string>(nullable: true),
                    FranchiseId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movies_Franchises_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MovieCharacters",
                columns: table => new
                {
                    MovieId = table.Column<int>(nullable: false),
                    CharacterId = table.Column<int>(nullable: false),
                    ActorId = table.Column<int>(nullable: false),
                    PictureLink = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovieCharacters", x => new { x.CharacterId, x.MovieId });
                    table.ForeignKey(
                        name: "FK_MovieCharacters_Actors_ActorId",
                        column: x => x.ActorId,
                        principalTable: "Actors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovieCharacters_Characters_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Characters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovieCharacters_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Actors",
                columns: new[] { "Id", "Biography", "DOB", "FirstName", "Gender", "LastName", "OtherNames", "PlaceOfBirth" },
                values: new object[,]
                {
                    { 1, "Biography1", new DateTime(1943, 8, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), "Robert", "Male", "De Niro", null, "Manhattan, New York City" },
                    { 2, "Biography2", new DateTime(1937, 4, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "Jack", "Male", "Nicholson", null, "Neptune, New Jersey" },
                    { 3, "Biography3", new DateTime(1924, 4, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "Marlon", "Male", "Brando", null, "Omaha, Nebraska" },
                    { 4, "Biography4", new DateTime(1954, 12, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "Denzel", "Male", "Washington", null, "Mount Vernon, New York" },
                    { 5, "Biography5", new DateTime(1907, 5, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), "Katharine", "Female", "Hepburn", null, "Hartford, Connecticut" },
                    { 6, "Biography6", new DateTime(1899, 12, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "Humphrey", "Male", "Bogart", null, "New York City, New York" },
                    { 7, "Biography7", new DateTime(1949, 6, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "Meryl", "Female", "Streep", null, "Summit, New Jersey" },
                    { 8, "Biography8", new DateTime(1957, 4, 29, 0, 0, 0, 0, DateTimeKind.Unspecified), "Daniel", "Male", "Day-Lewis", null, "Greenwhich, London" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "The marvel cinematic universe", "Marvel Cinematic Universe" },
                    { 2, "The D.C. cinematic universe", "D.C. Cinematic Universe" },
                    { 3, "The Tolkien cinematic universe", "Tolkien Cinematic Universe" },
                    { 4, "The Harry Potter movies", "J.K. Rowling Cinematic Universe" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "ImageLink", "ReleaseYear", "Title", "TrailerLink" },
                values: new object[,]
                {
                    { 1, "Louis Leterrier", 1, "Action, Adventure, Sci-Fi", null, 2008, "The incredible Hulk", null },
                    { 2, "Ang Lee", 1, "Action, Sci-Fi", null, 2003, "Hulk", null },
                    { 3, "Russo Brothers", 1, "Action, Adventure, Drama", null, 2019, "Avengers: Endgame", null },
                    { 4, "Russo Brothers", 1, "Action, Adventure Sci-Fi", null, 2018, "Avengers: Infinity War", null },
                    { 5, "Taika Waititi", 1, "Action, Adventure, Comedy", null, 2017, "Thor: Ragnarok", null },
                    { 6, "Kenneth Branagh", 1, "Action, Adventure Fantasy", null, 2011, "Thor", null },
                    { 7, "Alan Taylor", 1, "Action, Adventure, Fantasy", null, 2013, "Thor: The Dark World", null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacters_ActorId",
                table: "MovieCharacters",
                column: "ActorId");

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacters_MovieId",
                table: "MovieCharacters",
                column: "MovieId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_FranchiseId",
                table: "Movies",
                column: "FranchiseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MovieCharacters");

            migrationBuilder.DropTable(
                name: "Actors");

            migrationBuilder.DropTable(
                name: "Characters");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Franchises");
        }
    }
}

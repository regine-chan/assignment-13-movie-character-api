﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment_13___Movie_character_API.Dtos
{
    public class MovieCharacterDto
    {
        public MovieDto Movie { get; set; }
        public CharacterDto Character { get; set; }
        public ActorDto Actor { get; set; }
        public string PictureLink { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Assignment_13___Movie_character_API.Dtos
{
    public class CharacterDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string? Alias { get; set; }
        public string Gender { get; set; }
        public string ImageLink { get; set; }

    }
}

using Assignment_13___Movie_character_API.Models;
using AutoMapper;

namespace Assignment_13___Movie_character_API.Dtos
{
    public class ObjectProfile : Profile
    {
        public ObjectProfile()
        {
            CreateMap<Actor, ActorDto>().ReverseMap();
            CreateMap<Character, CharacterDto>().ReverseMap();
            CreateMap<Franchise, FranchiseDto>().ReverseMap();
            CreateMap<MovieCharacter, MovieCharacterDto>().ReverseMap();
            CreateMap<Movie, MovieDto>().ReverseMap();
        }
    }
}
﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment_13___Movie_character_API.Models
{
    public class FilmDbContext : DbContext
    {
        public DbSet<Actor> Actors { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<MovieCharacter> MovieCharacters { get; set; }

        public FilmDbContext(DbContextOptions opt) : base(opt) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MovieCharacter>().HasKey(mc => new { mc.CharacterId, mc.MovieId });

            modelBuilder.Entity<Actor>().HasData(new Actor { Id = 1, FirstName = "Robert", LastName = "De Niro", DOB = new DateTime(1943, 8, 17), Gender = "Male", PlaceOfBirth = "Manhattan, New York City", Biography = "Biography1" });
            modelBuilder.Entity<Actor>().HasData(new Actor { Id = 2, FirstName = "Jack", LastName = "Nicholson", DOB = new DateTime(1937, 4, 22), Gender = "Male", PlaceOfBirth = "Neptune, New Jersey", Biography = "Biography2" });
            modelBuilder.Entity<Actor>().HasData(new Actor { Id = 3, FirstName = "Marlon", LastName = "Brando", DOB = new DateTime(1924, 4, 3), Gender = "Male", PlaceOfBirth = "Omaha, Nebraska", Biography = "Biography3" });
            modelBuilder.Entity<Actor>().HasData(new Actor { Id = 4, FirstName = "Denzel", LastName = "Washington", DOB = new DateTime(1954, 12, 28), Gender = "Male", PlaceOfBirth = "Mount Vernon, New York", Biography = "Biography4" });
            modelBuilder.Entity<Actor>().HasData(new Actor { Id = 5, FirstName = "Katharine", LastName = "Hepburn", DOB = new DateTime(1907, 5, 12), Gender = "Female", PlaceOfBirth = "Hartford, Connecticut", Biography = "Biography5" });
            modelBuilder.Entity<Actor>().HasData(new Actor { Id = 6, FirstName = "Humphrey", LastName = "Bogart", DOB = new DateTime(1899, 12, 25), Gender = "Male", PlaceOfBirth = "New York City, New York", Biography = "Biography6" });
            modelBuilder.Entity<Actor>().HasData(new Actor { Id = 7, FirstName = "Meryl", LastName = "Streep", DOB = new DateTime(1949, 6, 22), Gender = "Female", PlaceOfBirth = "Summit, New Jersey", Biography = "Biography7" });
            modelBuilder.Entity<Actor>().HasData(new Actor { Id = 8, FirstName = "Daniel", LastName = "Day-Lewis", DOB = new DateTime(1957, 4, 29), Gender = "Male", PlaceOfBirth = "Greenwhich, London", Biography = "Biography8" });
            modelBuilder.Entity<Actor>().HasData(new Actor { Id = 9, FirstName = "Mark", LastName = "Ruffalo", DOB = new DateTime(1967, 11, 22), Gender = "Male", PlaceOfBirth = "Kenosha, Wisconsin", Biography = "Biography9" });
            modelBuilder.Entity<Actor>().HasData(new Actor { Id = 10, FirstName = "Edward", LastName = "Norton", DOB = new DateTime(1969, 8, 18), Gender = "Male", PlaceOfBirth = "Boston, Massachusetts", Biography = "Biography9" });
            modelBuilder.Entity<Actor>().HasData(new Actor { Id = 11, FirstName = "Eric", LastName = "Bana", DOB = new DateTime(1968, 8, 9), Gender = "Male", PlaceOfBirth = "Melbourne, Victoria", Biography = "Biography10" });


            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 1, Name = "Marvel Cinematic Universe", Description = "The marvel cinematic universe" });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 2, Name = "D.C. Cinematic Universe", Description = "The D.C. cinematic universe" });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 3, Name = "Tolkien Cinematic Universe", Description = "The Tolkien cinematic universe" });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 4, Name = "J.K. Rowling Cinematic Universe", Description = "The Harry Potter movies" });

            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 1, Title = "The incredible Hulk", Genre = "Action, Adventure, Sci-Fi", Director = "Louis Leterrier", FranchiseId = 1, ReleaseYear = 2008 });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 2, Title = "Hulk", Genre = "Action, Sci-Fi", Director = "Ang Lee", FranchiseId = 1, ReleaseYear = 2003 });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 3, Title = "Avengers: Endgame", Genre = "Action, Adventure, Drama", Director = "Russo Brothers", FranchiseId = 1, ReleaseYear = 2019 });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 4, Title = "Avengers: Infinity War", Genre = "Action, Adventure Sci-Fi", Director = "Russo Brothers", FranchiseId = 1, ReleaseYear = 2018 });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 5, Title = "Thor: Ragnarok", Genre = "Action, Adventure, Comedy", Director = "Taika Waititi", FranchiseId = 1, ReleaseYear = 2017 });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 6, Title = "Thor", Genre = "Action, Adventure Fantasy", Director = "Kenneth Branagh", FranchiseId = 1, ReleaseYear = 2011 });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 7, Title = "Thor: The Dark World", Genre = "Action, Adventure, Fantasy", Director = "Alan Taylor", FranchiseId = 1, ReleaseYear = 2013 });


            modelBuilder.Entity<Character>().HasData(new Character { Id = 1, Alias = "The Hulk", Gender = "Male", Name = "Bruce Banner" });

            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter { ActorId = 9, MovieId = 5, CharacterId = 1 });
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter { ActorId = 10, MovieId = 1, CharacterId = 1 });
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter { ActorId = 11, MovieId = 2, CharacterId = 1 });

        }



    }
}
